// Create Users
db.users.insertMany([

{
	firstName : "Seokjin",
	lastName : "Kim",
	email : "jin@gmail.com",
	password : "superTuna",
	isAdmin: false,
},
{
	firstName : "Yoongi",
	lastName : "Min",
	email : "agustd@gmail.com",
	password : "imAKing",
	isAdmin: false
},
{
	firstName : "Hoseok",
	lastName : "Jung",
	email : "uarmyhoped@gmail.com",
	password : "hopeWorld",
	isAdmin: false
},
{
	firstName : "Jimin",
	lastName : "Park",
	email : "j.m@gmail.com",
	password : "mochi",
	isAdmin: false
},
{
	firstName : "Taehyung",
	lastName : "Kim",
	email : "thv@gmail.com",
	password : "teamBear",
	isAdmin: false
},
{
	firstName : "Jungkook",
	lastName : "Jeon",
	email : "jungkook.97@gmail.com",
	password : "myEuphoria",
	isAdmin: false
}
])


// create courses
db.courses.insertMany([
{
	"name" : "HTML",
	"price" : "999",
	"isActive" : false
},
{
	"name" : "CSS",
	"price" : "1200",
	"isActive" : false
},
{
	"name" : "Javascript",
	"price" : "1600",
	"isActive" : false
}
])


// Read
db.users.find({isAdmin: false})


// Update 
	db.users.updateOne({},
	{
		$set: {
			isAdmin: true
		}
	}

)

	db.courses.updateOne({

		name: "Javascript",
	},
	{
		$set: {
			isAdmin: true
		}
	}

)

// Delete

db.courses.deleteMany({isActive: false})